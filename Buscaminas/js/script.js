//en /js
/* ESTADOS POSIBLES
    ENABLE
    DISABLE
    
*/
var Buscaminas = {
    //array que contiene las celdas
    myCampo: [],
    // 0: iniciado 1:perdido 2:ganado
    estadoJuego: 0,
    myAncho: 0,
    myAlto: 0,
    celdasRestantes: 0, //para ganar, arranca con cant.celdas - bombas

    iniciar: function (contenedor, ancho, alto, bombas) {
        var tipo = contenedor.substr(0, 1),
            contenedor = contenedor.substr(1, contenedor.length),
            eContenedor;
        if (tipo == ".") {
            eContenedor = document.getElementsByClassName(contenedor)[0];
        } else {
            eContenedor = document.getElementById(contenedor);
        }
        this.myAncho = ancho;
        this.myAlto = alto;
        this.celdasRestantes = (ancho * alto) - bombas;
        eContenedor.appendChild(this.generarPanelDeControl(bombas));
        eContenedor.appendChild(this.generarCampoDeMinas(ancho, alto));
        this.ponerBombas(bombas);
        this.calcularCercanas();
        //inicializo temporizador
        setInterval(Buscaminas.actualizaTemporizador, 1000);
    },
    
    actualizaTemporizador: function(){
        if(Buscaminas.estadoJuego == 0){
            contador = document.getElementsByClassName("temp-txt")[0];
            segundos = contador.innerHTML;
            segundos++;
            contador.innerHTML = segundos;
        }
    },

    generarCampoDeMinas: function (ancho, alto) {
        var eTabla = document.createElement("table"),
            eFila, eCelda;
        eTabla.setAttribute("class", "campoDeMinas");
        for (var i = 0; i < alto; i++) {
            eFila = document.createElement("tr");
            for (var j = 0; j < ancho; j++) {
                id = j + ancho * i;
                eCelda = document.createElement("td");
                eCelda.setAttribute("Class", "ENABLE");
                eCelda.setAttribute("id", id);
                eCelda.addEventListener("click", this.cambiarEstadoDeCelda);
                eCelda.addEventListener("contextmenu", this.toggleBloqueada);
                this.myCampo[id] = {
                    celda: eCelda,
                    cercanas: 0,
                    tieneBomba: false
                };
                eFila.appendChild(eCelda);
            }
            eTabla.appendChild(eFila);
        }
        return eTabla;
    },

    getCampo: function () {
        return this.myCampo;
    },

    cambiarEstadoDeCelda: function (evt) {
        if (Buscaminas.estadoJuego == 0) {
            celda = evt.currentTarget;
            id = celda.getAttribute("id");
            estado = celda.getAttribute("Class");
            campo = Buscaminas.getCampo();
            switch (estado) {
                case estado = 'ENABLE':
                    if (campo[id].tieneBomba) {
                        //perdio el juego
                        Buscaminas.mostrar(id);
                        setTimeout(Buscaminas.msgPerdio, 400);
                        Buscaminas.estadoJuego = 1;
                    } else {
                        Buscaminas.procesar(id);
                    }
                    break;
            }
        }
    },
    
    msgPerdio: function(){
        alert("Perdiste!");
    },
    
    msgGano: function(){
        alert("Ganaste!");
    },

    ponerBombas: function (bombas) {
        while (bombas !== 0) {
            nroCelda = Math.floor(Math.random() * this.myCampo.length);
            lCelda = this.myCampo[nroCelda];
            if (lCelda.tieneBomba == false) {
                lCelda.tieneBomba = true;
                bombas = bombas - 1;
            }
        }

    },

    calcularCercanas: function () {
        for (var i = 0; i < this.myCampo.length; i++) {
            contador = 0;
            cercanas = Buscaminas.getCercanas(i);
            for (j = 0; j < 8; j++) {
                if ((cercanas[j] >= 0) && (cercanas[j] <= this.myCampo.length - 1)) {
                    if (this.myCampo[cercanas[j]].tieneBomba) {
                        contador++;
                    }
                }
            }
            this.myCampo[i].cercanas = contador;
        }
    },

    getCercanas: function (i) {
        cercanas = [];
        izq = 0;
        izq_arriba = 0;
        arriba = 0;
        der_arriba = 0;
        der = 0;
        der_abajo = 0;
        abajo = 0;
        izq_abajo = 0;
        //  para sumar: a -(-b); sino me los concatena
        izq = (i - 1);
        izq_arriba = (i - (this.myAncho + 1));
        arriba = (i - this.myAncho);
        der_arriba = (i - (this.myAncho - 1));
        der = (i - (-1));
        der_abajo = (i - (-(this.myAncho + 1)));
        abajo = (i - (-this.myAncho));
        izq_abajo = (i - (-(this.myAncho - 1)));
        myPos = Buscaminas.getPos(i);
        switch (String(myPos)) {
            case "sup_izq":
                cercanas = [der, der_abajo, abajo];
                break;
            case "sup":
                cercanas = [der, der_abajo, abajo, izq_abajo, izq];
                break;
            case "sup_der":
                cercanas = [abajo, izq_abajo, izq];
                break;
            case "der":
                cercanas = [abajo, izq_abajo, izq, izq_arriba, arriba];
                break;
            case "inf_der":
                cercanas = [izq, izq_arriba, arriba];
                break;
            case "inf":
                cercanas = [izq, izq_arriba, arriba, der_arriba, der];
                break;
            case "inf_izq":
                cercanas = [arriba, der_arriba, der];
                break;
            case "izq":
                cercanas = [arriba, der_arriba, der, der_abajo, abajo];
                break;
            case "centro":
                cercanas = [arriba, der_arriba, der, der_abajo, abajo, izq_abajo, izq, izq_arriba];
                break;
        }
        return cercanas;
    },

    procesar: function (id) {
        if (this.myCampo[id].cercanas == 0) {
            Buscaminas.descubrir(id);
            //console.log(Buscaminas.getPos(id));
        } else {
            Buscaminas.mostrar(id);
        }
    },
    descubrir: function (id) {
        if ((this.myCampo[id].cercanas > 0) && (!this.myCampo[id].tieneBomba)) {
            Buscaminas.mostrar(id);
        } else if ((this.myCampo[id].cercanas == 0) && (!this.myCampo[id].tieneBomba)) {
            Buscaminas.mostrar(id);
            cercanas = Buscaminas.getCercanas(id);
            //console.log(cercanas);
            for (var idCelda of cercanas) {
                if ((document.getElementById(idCelda)).getAttribute("Class") == "ENABLE") {
                    Buscaminas.descubrir(idCelda);
                }
            }
        }
    },

    getPos: function (id) {
        if (id < this.myAncho) {
            //esta en la fila superior
            if (id == 0) {
                //esta en la esquina superior izquierda
                return "sup_izq";
            } else if ((id - (-1)) == this.myAncho) {
                //esta en la esquina superior derecha
                return "sup_der";
            } else {
                //esta arriba
                return "sup";
            }
        } else if (((id - (-1)) % this.myAncho) == 0) {
            //esta en la columa derecha
            if (id == this.myCampo.length - 1) {
                //esta en la esquina inferior derecha
                return "inf_der";
            } else {
                //esta a la derecha
                return "der";
            }
        } else if (id >= (this.myAncho * (this.myAlto - 1))) {
            //esta en la fila inferior
            if (id == (this.myAncho * (this.myAlto - 1))) {
                //esta en la esquina inferior izquierda
                return "inf_izq";
            } else if (id == this.myCampo.length - 1) {
                //esta en la esquina inferior derecha
                return "inf_der";
            } else {
                //esta abajo
                return "inf";
            }
        } else if (id % this.myAncho == 0) {
            //esta en la columna izquierda
            if (id == 0) {
                //esta en la esquina superior izquierda
                return "sup_izq";
            } else if (id == (this.myAncho * (this.myAlto - 1))) {
                //esta en la esquina inferior izquierda
                return "inf_izq";
            } else {
                //esta a la izquierda
                return "izq"
            }
        } else {
            //no esta en ningun borde
            console.log(((id - (-1)) % this.myAncho));
            return "centro";
        }
    },


    mostrar: function (id) {
        bombasCercanas = this.myCampo[id].cercanas;
        switch (bombasCercanas) {
            case bombasCercanas = 0:
                this.myCampo[id].celda.setAttribute("Class", "BOMBAS0");
                break;

            case bombasCercanas = 1:
                this.myCampo[id].celda.setAttribute("Class", "BOMBAS1");
                this.myCampo[id].celda.innerHTML = "1";
                break;

            case bombasCercanas = 2:
                this.myCampo[id].celda.setAttribute("Class", "BOMBAS2");
                this.myCampo[id].celda.innerHTML = "2";
                break;

            case bombasCercanas = 3:
                this.myCampo[id].celda.setAttribute("Class", "BOMBAS3");
                this.myCampo[id].celda.innerHTML = "3";
                break;

            case bombasCercanas = 4:
                this.myCampo[id].celda.setAttribute("Class", "BOMBAS4");
                this.myCampo[id].celda.innerHTML = "4";
                break;

            case bombasCercanas = 5:
                this.myCampo[id].celda.setAttribute("Class", "BOMBAS5");
                this.myCampo[id].celda.innerHTML = "5";
                break;

            case bombasCercanas = 6:
                this.myCampo[id].celda.setAttribute("Class", "BOMBAS6");
                this.myCampo[id].celda.innerHTML = "6";
                break;

            case bombasCercanas = 7:
                this.myCampo[id].celda.setAttribute("Class", "BOMBAS7");
                this.myCampo[id].celda.innerHTML = "7";
                break;

            case bombasCercanas = 8:
                this.myCampo[id].celda.setAttribute("Class", "BOMBAS8");
                this.myCampo[id].celda.innerHTML = "8";
                break;
        }
        if (this.myCampo[id].tieneBomba) {
            this.myCampo[id].celda.setAttribute("Class", "DETONADA");
            this.myCampo[id].celda.innerHTML = "X";
        } else {
            Buscaminas.celdasRestantes--;
            Buscaminas.checkGano();
        }
    },

    toggleBloqueada: function (evt) {
        if (Buscaminas.estadoJuego == 0){
            celda = evt.currentTarget;
            id = celda.getAttribute("id");
            estado = celda.getAttribute("class");
            campo = Buscaminas.getCampo();
            switch (estado){
                case estado = 'ENABLE':
                    //bloquear celda
                    celda.setAttribute("class", "LOCKED");
                    marcadorBombas = document.getElementsByClassName("cant-bombas")[0];
                    cont = marcadorBombas.innerHTML;
                    cont--;
                    marcadorBombas.innerHTML = cont;
                    
                    break;
                case estado = 'LOCKED':
                    //desbloquear celda
                    celda.setAttribute("class", "ENABLE");
                    marcadorBombas = document.getElementsByClassName("cant-bombas")[0];
                    cont = marcadorBombas.innerHTML;
                    cont++;
                    marcadorBombas.innerHTML = cont;
                    break;
            }
        }
    },
    
    checkGano: function(){
        if (Buscaminas.celdasRestantes == 0){
            setTimeout(Buscaminas.msgGano, 400);
            Buscaminas.estadoJuego = 2;
        }
    },

    generarPanelDeControl: function (bombas) {
        var ePanel = document.createElement("div");
        ePanel.setAttribute("class", "panelDeControl");
        ePanel.appendChild(this.generarContadorDeMinas(bombas));
        ePanel.appendChild(this.generarBotonInicio());
        ePanel.appendChild(this.generarTemporizador());
        return ePanel;
    },

    generarTemporizador: function () {
        var eTempo = document.createElement("div"),
            eContador = document.createElement("p"),
            tContador = document.createTextNode("00");
        eContador.setAttribute("class", "temp-txt");
        eContador.appendChild(tContador);
        eTempo.appendChild(eContador);
        eTempo.setAttribute("class", "temporizador");
        return eTempo;
    },

    reiniciar: function () {
        //reiniciar
        document.location.reload();
    },

    generarBotonInicio: function () {
        var eInicio = document.createElement("div");
        eImagen = document.createElement("img");
        eImagen.setAttribute("src", "images/smile.jpg");
        eInicio.addEventListener("click", this.reiniciar);
        eInicio.appendChild(eImagen);
        eInicio.setAttribute("class", "botonInicio");
        return eInicio;
    },

    generarContadorDeMinas: function (bombas) {
        var eMinas = document.createElement("div"),
            eContador = document.createElement("p"),
            tContador = document.createTextNode(bombas);
        eContador.setAttribute("class", "cant-bombas");
        eContador.appendChild(tContador);
        eMinas.appendChild(eContador);
        eMinas.setAttribute("class", "contadorDeMinas");
        return eMinas;
    }
}






/*
    getPos: function (id) {
            switch (id) {

                case id < this.myAncho:
                    //esta en la fila superior
                    if (id % this.myAncho == 0) {
                        //esta en la esquina superior izquierda
                        return "sup_izq";
                    } else if (id + 1 % this.myAncho == 0) {
                        //esta en la esquina superior derecha
                        return "sup_der";
                    } else {
                        //esta arriba
                        return "sup";
                    }
                    break;

                case (id + 1) % this.myAncho == 0:
                    //esta en la columa derecha
                    if (id == this.myAncho - 1) {
                        //esta en la esquina superior derecha
                        return "sup_der";
                    } else if (id == this.myCampo.length - 1) {
                        //esta en la esquina inferior derecha
                        return "inf_der";
                    } else {
                        //esta a la derecha
                        return "der";
                    }
                    break;

                case id >= (this.myAncho * (this.myAlto - 1)):
                    //esta en la fila inferior
                    if (id == (this.myAncho * (this.myAlto - 1))) {
                            //esta en la esquina inferior izquierda
                            return "inf_izq";
                        } else if (id == this.myCampo.length - 1) {
                            //esta en la esquina inferior derecha
                            return "inf_der";
                        } else {
                            //esta abajo
                            return "inf";
                        }
                        break;

                case id % this.myAncho == 0:
                    //esta en la columna izquiera
                    if (id == 0) {
                        //esta en la esquina superior izquierda
                        return "sup_izq";
                    } else if (id == (this.myAncho * (this.myAlto - 1))) {
                        //esta en la esquina inferior izquierda
                        return "inf_izq";
                    } else {
                        //esta a la izquierda
                        return "izq;"
                    }
                    break;
                    
                default:
                    return "centro";
                    break;
                    }
            },
*/
