var Criptograma = {
    frases: [
        "la historia la escriben los vencedores",
        "cada pueblo tiene el gobierno que se merece",
        "busca tu felicidad sin lastimar a nadie",
        "los sueños son las alas que te ayudan a volar",
        "no es la realidad concreta de un objeto lo que influye en nuestra personalidad sino la realidad interpretada"
    ],
    codigos: [
        //numeros
        ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26"],
        //letras
        ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"],
        //dibujos
        ["\u231A", "\u231B","\u23F0", "\u2614", "\u2615", "\u261D", "\u2652", "\u2658", "\u265B", "\u2665", "\u266B", "\u267F", "\u26A1", "\u2693", "\u26BD", "\u26C4", "\u26C5", "\u26D4", "\u26F2", "\u26F3", "\u26F5", "\u26FD", "\u26F9", "\u26FA", "\u270B", "\u2728", "\u27B0",]
    ],
    abecedario: ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"],
    estado: "inicio", //inicio-jugando-fin,
    indexCodigo: 0, //indice del codigo selecionado
    indexFrase: 2, //indice de la frase seleccionada
    clave: [], // par {letra, valor}. Ej: {letra: a, valor: 11}
    codificada: [], //par {letra, valor} letra = letra que le asigno el usuario y valor es el codigo

    //abecedario español tiene 27 letras

    panelConfig: function (contenedor) {
        var tipo = contenedor.substr(0, 1),
            contenedor = contenedor.substr(1, contenedor.length);
        if (tipo == ".") {
            contenedor = document.getElementsByClassName(contenedor)[0];
        } else {
            contenedor = document.getElementById(contenedor);
        }

        btnLetras = document.createElement("button");
        btnLetras.setAttribute("type", "button");
        btnLetras.setAttribute("class", "btn-not-selected");
        btnLetras.innerHTML = "Letras";
        btnLetras.addEventListener("click", function () {
            Criptograma.setLetras();
            myContainer = document.getElementsByClassName("panelConfig")[0];
            myContainer.remove();
            Criptograma.iniciar(contenedor);
        }, false);

        btnNumeros = document.createElement("button");
        btnNumeros.setAttribute("type", "button");
        btnNumeros.setAttribute("class", "btn-selected");
        btnNumeros.innerHTML = "Números";
        btnNumeros.addEventListener("click", function () {
            Criptograma.setNumeros();
            myContainer = document.getElementsByClassName("panelConfig")[0];
            myContainer.remove();
            Criptograma.iniciar(contenedor);
        }, false);
        
        btnDibujos = document.createElement("button");
        btnDibujos.setAttribute("type", "button");
        btnDibujos.setAttribute("class", "btn-not-selected");
        btnDibujos.innerHTML = "Dibujos";
        btnDibujos.addEventListener("click", function () {
            Criptograma.setDibujos();
            myContainer = document.getElementsByClassName("panelConfig")[0];
            myContainer.remove();
            Criptograma.iniciar(contenedor);
        }, false);

        container = document.createElement("div");
        container.setAttribute("class", "panelConfig");
        container.appendChild(btnLetras);
        container.appendChild(btnNumeros);
        container.appendChild(btnDibujos);

        contenedor.appendChild(container);
    },

    setLetras: function (evt) {
        Criptograma.indexCodigo = 1;
    },

    setNumeros: function () {
        Criptograma.indexCodigo = 0;
    },
    
    setDibujos: function(){
        Criptograma.indexCodigo = 2;
    },

    iniciar: function (eContenedor) {
        //set indexCodigo
        //set indexFrase
        //generar clave (asigna random de codigo a cada letra)
        //selecciono frase al azar
        Criptograma.indexFrase = Math.floor(Math.random() * Criptograma.frases.length);
        //genero la clave
        Criptograma.generarClave();
        //generar el panel de codigo (donde el usuario asigna valor a letra)
        eContenedor.appendChild(Criptograma.generarCodigo());
        //codifica frase segun clave generada
        Criptograma.codificada = Criptograma.codificarFrase();
        //generar el contenedor de la frase
        eContenedor.appendChild(Criptograma.generarFrase());
        Criptograma.estado = "jugando";
    },

    generarClave: function () {
        var disponibles = Criptograma.codigos[Criptograma.indexCodigo].slice();
        for (letraAbecedario of Criptograma.abecedario) {
            index = Math.floor(Math.random() * disponibles.length);
            valor = disponibles[index];
            Criptograma.clave.push({
                letra: letraAbecedario,
                valor: valor
            });
            disponibles.splice(index, 1);
        }
    },

    codificarFrase: function () {
        var frase = [];
        for (var i = 0; i < Criptograma.frases[Criptograma.indexFrase].length; i++) {
            letra = Criptograma.frases[Criptograma.indexFrase].substr(i, 1);
            valor = Criptograma.codificarLetra(letra);
            /*if (valor == letra) {
                letraCodigo = {
                    letra: letra,
                    valor: valor
                };
            } else {
                letraCodigo = {
                    letra: "",
                    valor: valor
                };
            }*/
            letraCodigo = {
                letra: "",
                valor: valor
            };

            frase.push(letraCodigo);
        }
        return frase;
    },

    codificarLetra: function (char) {
        for (var j = 0; j < Criptograma.abecedario.length; j++) {
            if (Criptograma.clave[j].letra == char) {
                return Criptograma.clave[j].valor;
            }
        }
        return char;
    },

    generarCodigo: function () {
        //codigo: 27 * letra valor
        var codigo = document.createElement("div");
        codigo.setAttribute("class", "codigo");


        for (valor of Criptograma.abecedario) {
            //simbolo valor: letra + valor asignado por jugador
            //hacer uno por letra
            letraValor = document.createElement("div");
            letraValor.setAttribute("class", "letra-valor");
            //creo elemento letra
            letra = document.createElement("p");
            letra.setAttribute("class", "letra");
            letra.setAttribute("name", "-");
            letra.innerHTML = valor;
            //creo elemento valor
            lista = Criptograma.generarListaAbecedario();
            lista.setAttribute("class", "valor-" + valor);
            lista.addEventListener("change", Criptograma.cambioLetra, false);
            letraValor.appendChild(letra);
            letraValor.appendChild(lista);
            codigo.appendChild(letraValor);
        }

        return codigo;
    },

    cambioLetra: function (evt) {
        var lista = evt.currentTarget;
        letra = lista.getAttribute("class").split("-")[1];
        newValor = lista.options[lista.selectedIndex].value;
        if (Criptograma.estado == "jugando") {
            Criptograma.actualizarFrase(letra, newValor);
            if (Criptograma.gano()) {
                estado = "fin";
                setTimeout(Criptograma.msgGano, 400);
            }
        }
    },
    msgGano: function() {
        alert("Ganaste!");
    },

    gano: function () {
        for (var i = 0; i < Criptograma.codificada.length; i++) {
            if (Criptograma.charValido(i)) {
                if (Criptograma.codificada[i].letra !== Criptograma.frases[Criptograma.indexFrase].substr(i, 1)) {
                    return false;
                }
            }
        }
        return true;
    },

    charValido: function (i) {
        //true si char es parte del mensaje, false si es una coma, espacio o punto
        if (Criptograma.frases[Criptograma.indexFrase].substr(i, 1) == " ") {
            return false;
        }
        if (Criptograma.frases[Criptograma.indexFrase].substr(i, 1) == ",") {
            return false;
        }
        if (Criptograma.frases[Criptograma.indexFrase].substr(i, 1) == ".") {
            return false;
        }
        return true;
    },

    actualizarFrase: function (letra, newValor) {
        //buscar valores viejos de letra y borrarlos(vista)
        aViejo = document.getElementsByName(letra);
        arr = Array.from(aViejo);
        for (viejo of arr) {
            console.log(aViejo.length);
            viejo.innerHTML = "";
            viejo.setAttribute("name", "-");
        }
        //buscar valores viejos de letra y borrarlos(logico)
        for (el of Criptograma.codificada) {
            if (el.letra == letra) {
                el.letra == "";
            }
        }
        //pisar valores (logico)
        aCambiar = document.getElementsByClassName("cod-" + newValor);
        for (elem of Criptograma.codificada) {
            if (elem.valor == newValor) {
                elem.letra = letra;
            }
        }
        //pisar valores (vista)
        for (e of aCambiar) {
            e.innerHTML = letra;
            e.setAttribute("name", letra);
        }
    },

    generarListaAbecedario: function () {
        list = document.createElement("select");
        //agrego opcion vacia
        opt = new Option("", "none");
        list.appendChild(opt);
        //tuve que cambiar a valor 2 porque se pisaba con el valor de generarCodigo()
        for (valor2 of Criptograma.codigos[Criptograma.indexCodigo]) {
            opt = new Option(valor2, valor2);
            list.appendChild(opt);
        }
        return list;
    },

    generarFrase: function () {
        var panelFrase = document.createElement("div");
        panelFrase.setAttribute("class", "contenedor-frase");
        for (var i = 0; i < Criptograma.codificada.length; i++) {
            letraCodigo = document.createElement("div");
            letraCodigo.setAttribute("class", "letra-codigo");

            letra = document.createElement("p");
            letra.setAttribute("class", "cod-" + Criptograma.codificada[i].valor);
            letra.innerHTML = Criptograma.codificada[i].letra;

            codigo = document.createElement("p");
            codigo.setAttribute("class", "cod-abajo");
            codigo.innerHTML = Criptograma.codificada[i].valor;

            letraCodigo.appendChild(letra);
            letraCodigo.appendChild(codigo);
            panelFrase.appendChild(letraCodigo);
        }

        return panelFrase;
    }
}
