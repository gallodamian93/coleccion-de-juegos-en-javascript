var EnLinea = {
    tablero: [],
    jug: [],
    turno: "",
    size: 0,
    linea: 0,
    //estados: inicio-jugando-fin
    estado: "inicio",

    showPanel: function (contenedor) {
        var tipo = contenedor.substr(0, 1),
            contenedor = contenedor.substr(1, contenedor.length),
            eContenedor;
        if (tipo == ".") {
            eContenedor = document.getElementsByClassName(contenedor)[0];
        } else {
            eContenedor = document.getElementById(contenedor);
        }
        eContenedor.appendChild(this.generarPanel());

    },

    generarPanel: function () {
        var eForm = document.createElement("form"),
            eSelectSize = document.createElement("select"),
            eSelectLinea = document.createElement("select"),
            eInputP1 = document.createElement("input"),
            labelP1 = document.createElement("label"),
            eInputP2 = document.createElement("input"),
            labelP2 = document.createElement("label"),
            eSubmit = document.createElement("button");
        eForm.setAttribute("class", "panel-de-configuracion");
        eSelectSize.setAttribute("class", "select-size");
        eSelectLinea.setAttribute("class", "select-linea");
        eInputP1.setAttribute("class", "input-p1");
        eInputP1.setAttribute("id", "p1");
        eInputP2.setAttribute("class", "input-p2");
        eInputP1.setAttribute("id", "p2");
        labelP1.setAttribute("for", "p1");
        labelP1.innerHTML = "Jugador 1:";
        labelP2.setAttribute("for", "p2");
        labelP2.innerHTML = "Jugador 2:";
        eSubmit.innerHTML = "Jugar!";
        eSubmit.setAttribute("type", "button");
        eSubmit.addEventListener("click", this.iniciar);

        //cargar opciones del size
        for (var i = 3; i < 11; i++) {
            opt = new Option(i + "X" + i, i.toString());
            eSelectSize.appendChild(opt);
        }
        //cargar opciones de línea
        for (var i = 3; i < 11; i++) {
            opt = new Option(i + " en linea", i.toString());
            eSelectLinea.appendChild(opt);
        }
        eForm.appendChild(labelP1);
        eForm.appendChild(eInputP1);
        eForm.appendChild(labelP2);
        eForm.appendChild(eInputP2);
        eForm.appendChild(eSelectSize);
        eForm.appendChild(eSelectLinea);
        eForm.appendChild(eSubmit);
        return eForm;
    },

    iniciar: function () {
        nameP1 = document.getElementsByClassName("input-p1")[0].value;
        nameP2 = document.getElementsByClassName("input-p2")[0].value;
        mySize = document.getElementsByClassName("select-size")[0].value;
        myLinea = document.getElementsByClassName("select-linea")[0].value;
        if (EnLinea.valido(nameP1, nameP2, mySize, myLinea)) {
            //iniciar
            EnLinea.loadGame();
        } else {
            alert("Error de datos");
        }
    },

    loadGame: function () {
        //remuevo panel de configuracion
        form = document.getElementsByClassName("panel-de-configuracion")[0];
        form.remove();
        contenedor = document.getElementsByClassName("contenedor-juego")[0];
        //agrego panel de datos
        contenedor.append(EnLinea.generarPanelDeDatos());
        //agrego tablero(logico y vista)
        contenedor.append(EnLinea.generarTablero());

        EnLinea.cambiarTurno();
        EnLinea.estado = "jugando";
    },

    cambiarTurno: function () {
        if (EnLinea.turno == "") {
            //recien arranca, comienza el jugador 1
            EnLinea.turno = 1;
            EnLinea.refreshTurno();
        } else if (EnLinea.turno == 1) {
            //cambia turno 
            EnLinea.turno = 2;
            EnLinea.refreshTurno();
        } else if (EnLinea.turno == 2) {
            EnLinea.turno = 1;
            EnLinea.refreshTurno();
        }
    },

    refreshTurno: function () {
        turno = document.getElementsByClassName("turno")[0];
        turno.innerHTML = "Turno de " + EnLinea.jug[EnLinea.turno].name;
    },

    generarTablero: function () {
        tablero = document.createElement("table");
        tablero.setAttribute("class", "tablero");
        //tablero = table de size X size
        //creo tablero logico
        for (var i = 0; i < EnLinea.size; i++) {
            EnLinea.tablero[i] = [];
            for (var j = 0; j < EnLinea.size; j++) {
                EnLinea.tablero[i][j] = "libre";
            }
            //EnLinea.tablero[i] = ["libre", "libre", "libre"];
        }
        //creo tablero vista
        for (var i = 0; i < EnLinea.size; i++) {
            fila = document.createElement("tr");
            for (var j = 0; j < EnLinea.size; j++) {
                celda = document.createElement("td");
                celda.setAttribute("class", "libre");
                celda.setAttribute("id", i + "-" + j);
                celda.addEventListener("click", EnLinea.procesar);
                fila.appendChild(celda);
            }
            tablero.appendChild(fila);
        }

        return tablero;
    },

    procesar: function (evt) {
        if (EnLinea.estado == "jugando") {
            //obtengo id logico de la celda
            celda = evt.currentTarget;
            fila = celda.getAttribute("id").split("-")[0];
            columna = celda.getAttribute("id").split("-")[1];
            //verifico que este libre
            if (EnLinea.tablero[fila][columna] == "libre") {
                //esta libre, se procesa la jugada
                EnLinea.marcarCelda(fila, columna);
                //verificar resultado
                if (EnLinea.gano()) {
                    //gano
                    EnLinea.estado = "fin";
                    EnLinea.sumarMarcador();
                    //hago un pequeño delay porque el alert frena la ejecucion del codigo,
                    //podria ponerlo al final de sumarMarcador pero tiene mas sentido que 
                    //este aca
                    setTimeout(EnLinea.msgGano, 400);

                } else {
                    //sigue el juego
                    //fin de turno
                    EnLinea.cambiarTurno();
                }

            }

        }

    },

    msgGano: function () {
        alert("Gano " + EnLinea.jug[EnLinea.turno].name);
    },

    sumarMarcador: function () {
        //actualizo marcador logico
        EnLinea.jug[EnLinea.turno].won++;
        //actualizo vista
        marcador = document.getElementsByClassName("ganadas-p" + EnLinea.turno)[0];
        marcador.innerHTML = "Ganadas: " + EnLinea.jug[EnLinea.turno].won;
    },

    gano: function () {
        //verifico si gano quien puso la ficha- Si p1 tiro, p2 tiene cero posibilidades de ganar
        //chequear horizontales
        for (var i = 0; i < EnLinea.size; i++) {
            count = Number(0);
            for (var j = 0; j < EnLinea.size; j++) {
                if (EnLinea.tablero[i][j] == EnLinea.turno) {
                    count++;
                    if (count >= EnLinea.linea) {
                        //gano
                        return true;
                    }
                } else {
                    count = 0;
                }
            }
        }
        //chequear verticales
        for (var i = 0; i < EnLinea.size; i++) {
            count = Number(0);
            for (var j = 0; j < EnLinea.size; j++) {
                if (EnLinea.tablero[j][i] == EnLinea.turno) {
                    count++;
                    if (count >= EnLinea.linea) {
                        //gano
                        return true;
                    }
                } else {
                    count = 0;
                }
            }
        }
        //chequear oblicuas
        for (var k = EnLinea.size - 2; k > -EnLinea.size + 1; k--) {
            diagonalIzq = [];
            diagonalDer = [];
            for (var i = 0; i < EnLinea.size; i++) {
                for (var j = 0; j < EnLinea.size; j++) {
                    if (i - j == k) {
                        diagonalIzq.push(EnLinea.tablero[i][j]);
                    }
                    if (i + j == k + EnLinea.size - 1) {
                        diagonalDer.push(EnLinea.tablero[i][j]);
                    }
                }
            }
            //analizar diagonales

            if (EnLinea.tieneOblicua(diagonalIzq)) {
                return true;
            }
            if (EnLinea.tieneOblicua(diagonalDer)) {
                return true;
            }
        }
        return false;
    },

    tieneOblicua(diagonal) {
        count = Number(0);
        for (var i = 0; i < diagonal.length; i++) {
            if (diagonal[i] == EnLinea.turno) {
                count++;
                if (count >= EnLinea.linea) {
                    return true;
                }
            } else {
                count = 0;
            }
        }
        return false;
    },

    marcarCelda: function (fila, columna) {
        EnLinea.tablero[fila][columna] = EnLinea.turno;
        EnLinea.refreshCelda(fila + "-" + columna);
    },

    refreshCelda: function (id) {
        celda = document.getElementById(id);
        celda.setAttribute("class", "celda-" + EnLinea.turno);
    },

    //resetea celda logica y vista
    resetCelda: function (fila, columna) {
        EnLinea.tablero[fila][columna] = "libre";
        celda = document.getElementById(fila + "-" + columna);
        celda.setAttribute("class", "libre");
    },

    generarPanelDeDatos: function () {
        panelDatos = document.createElement("div");
        panelDatos.setAttribute("class", "panel-de-datos");
        //arranco con el panel de datos (contiene ganadas p1 y p2, y turno)
        //agrego los jugadores
        for (var i = 1; i <= 2; i++) {
            jugador = document.createElement("div");
            jugador.setAttribute("class", "panel-p" + i);
            nombre = document.createElement("p");
            nombre.setAttribute("class", "nombre-p" + i)
            nombre.innerHTML = EnLinea.jug[i].name;
            ganadas = document.createElement("p");
            ganadas.setAttribute("class", "ganadas-p" + i);
            ganadas.innerHTML = "Ganadas: " + EnLinea.jug[i].won;
            jugador.appendChild(nombre);
            jugador.appendChild(ganadas);
            panelDatos.appendChild(jugador);
        }

        //agrego el turno actual
        turno = document.createElement("p");
        turno.setAttribute("class", "turno");
        turno.innerHTML = "Turno de " + EnLinea.turno;
        panelDatos.appendChild(turno);

        //agrego el boton de reiniciar
        reboot = document.createElement("button");
        reboot.setAttribute("type", "button");
        reboot.setAttribute("class", "btn-reiniciar");
        reboot.innerHTML = "Reiniciar";
        reboot.addEventListener("click", EnLinea.reiniciar);
        panelDatos.appendChild(reboot);

        return panelDatos;
    },

    reiniciar: function () {
        for (var i = 0; i < EnLinea.size; i++) {
            for (var j = 0; j < EnLinea.size; j++) {
                //resetea celda logica y vista
                EnLinea.resetCelda(i, j);
            }
        }
        EnLinea.estado = "jugando";
        EnLinea.cambiarTurno();
    },

    valido: function (nameP1, nameP2, size, linea) {
        //valida y siendo correcto guarda los datos
        nameP1 = EnLinea.sanitize(nameP1);
        nameP2 = EnLinea.sanitize(nameP2);
        mySize = Number(size);
        myLinea = Number(linea);
        if ((nameP1.length < 1) || (nameP2.length < 1)) {
            //nombres vacios
            return false;
        }
        if ((mySize < 3) || (mySize > 10) || (myLinea < 3) || (myLinea > 10)) {
            //rango de tablero o linea incorrectos
            return false;
        }
        if (myLinea > mySize) {
            // linea > tablero 
            return false;
        }

        //me guardo los datos
        EnLinea.size = mySize;
        EnLinea.linea = myLinea;
        EnLinea.jug[1] = {
            nro: 1,
            name: nameP1,
            won: 0
        };
        EnLinea.jug[2] = {
            nro: 2,
            name: nameP2,
            won: 0
        };
        return true;
    },

    sanitize: function (str) {
        str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim, "");
        return str.trim();
    }


}
