//agregar control de preguntas a realizar > cantidad de preguntas existentes

var Test = {
    oTest: new Object(),
    restantes: [],
    realizadas: [],
    respuestas: [],
    //0: iniciado, 1: finalizado
    estado: 0,

    iniciar: function (contenedor) {
        var tipo = contenedor.substr(0, 1),
            contenedor = contenedor.substr(1, contenedor.length),
            sTest;
        if (tipo == ".") {
            contenedor = document.getElementsByClassName(contenedor)[0];
        } else {
            contenedor = document.getElementById(contenedor);
        }
        //conseguir el test del archivo
        sTest = Test.loadTestFile();
        //convertirlo a objeto
        Test.oTest = JSON.parse(sTest);
        if (Test.oTest == null) {
            alert("No se puedo cargar el test");
        } else {
            //cargó el test, cargo el panel del test
            Test.generarTest(contenedor);
        }
    },

    loadTestFile: function () {
        var result = null;
        var xmlhttp = new XMLHttpRequest();
        //requiero hacer query al servidor (esto se ejecuta desde el cliente)
        xmlhttp.open("GET", "http://localhost/PAW/TP3/Test/json/config.json", false);
        xmlhttp.send();
        if (xmlhttp.status == 200) {
            result = xmlhttp.responseText;
        }
        return result;
    },

    generarTest: function (contenedor) {
        var eTitulo = document.createElement("p"),
            eTimer = document.createElement("p");
        //agrego el título del test al principio así no me olvido
        eTitulo.setAttribute("class", "titulo-test");
        eTitulo.innerHTML = Test.oTest.titulo;
        contenedor.appendChild(eTitulo);
        //agrego el timer
        eTimer.setAttribute("class", "timer");
        eTimer.innerHTML = "0";
        contenedor.appendChild(eTimer);
        //inicializo restantes(preguntas que se pueden agregar al test, si ya se puso se elimina. [{pregunta, selected}]
        for (var preg of Test.oTest.preguntas) {
            Test.restantes.push(preg);
        }
        //genero el panel de cada pregunta: pregunta + opciones
        for (var i = 0; i < Test.oTest.cantidadAPreguntar; i++) {
            contenedor.appendChild(Test.generarPregunta());
        }
        //genero el botón de submit
        contenedor.appendChild(Test.generarSubmit());
        //inicializo el timer
        setInterval(Test.actualizaTimer, 1000);

    },

    actualizaTimer: function () {
        if (Test.estado == 0) {
            timer = document.getElementsByClassName("timer")[0];
            cont = timer.innerHTML;
            cont++;
            timer.innerHTML = cont;
            if (cont == Test.oTest.tiempoDeTrabajo) {
                //se acabo el tiempo
                Test.estado = 1;
                setTimeout(function () {
                    alert("Se acabo el tiempo!");
                }, 400);
            }
        }
    },

    //generar el boton
    generarSubmit: function () {
        var btn = document.createElement("button");
        btn.setAttribute("type", "button");
        btn.innerHTML = "Ver resultados";
        btn.setAttribute("class", "btn-submit");
        btn.addEventListener("click", Test.submit);
        return btn;
    },

    //apretó el botón
    submit: function () {
        if (Test.estado == 0) {
            var bGano = Test.gano();
            if (bGano) {
                alert("ganaste");
            } else {
                alert("perdiste");
            }
            Test.estado = 1;
        }
    },

    gano: function () {
        for (preg of Test.realizadas) {
            index = Test.realizadas.indexOf(preg);
            if (!Test.bienRespondida(preg.correctas, Test.respuestas[index])) {
                return false;
            }
        }
        return true;
    },

    bienRespondida: function (respuestas, seleccionadas) {
        if (respuestas.length !== seleccionadas.length) {
            return false;
        }
        for (r of respuestas) {
            var esta = false;
            for (s of seleccionadas) {
                if (r == s) {
                    esta = true;
                }
            }
            if (!esta) {
                return false;
            }
        }
        return true;
    },

    generarPregunta: function () {
        var panelPregunta = document.createElement("div");

        //obtengo índice de pregunta a agregar (de restantes)
        indexPregunta = Math.floor(Math.random() * Test.restantes.length);
        //obtengo la pregunta del test
        var oPregunta = Test.restantes[indexPregunta];
        //la agrego a la lista de realizadas
        Test.realizadas.push(oPregunta);
        //la elimino de restantes
        Test.restantes.splice(indexPregunta, 1);
        //obtengo el indice real de la pregunta (en archivo json)
        indexPreguntaReal = Test.oTest.preguntas.indexOf(oPregunta);
        //agrego entrada en respuestas
        Test.respuestas[Test.realizadas.length - 1] = [];
        //
        //titulo
        var ePregunta = document.createElement("p");
        ePregunta.setAttribute("class", "pregunta");
        ePregunta.setAttribute("id", "p-" + indexPreguntaReal);
        ePregunta.innerHTML = oPregunta.pregunta;
        panelPregunta.appendChild(ePregunta);
        //respuestas
        for (var op of oPregunta.respuestas) {
            var eRespuesta = document.createElement("input");
            //agrego respuesta(de tipo checkbox porque el test admite multiples respuestas correctas)
            eRespuesta.setAttribute("type", "checkbox");
            eRespuesta.setAttribute("class", "rb-respuesta");
            eRespuesta.setAttribute("name", "pregunta-" + Test.realizadas.indexOf(oPregunta));
            eRespuesta.setAttribute("id", "r-" + indexPreguntaReal);
            eRespuesta.setAttribute("value", oPregunta.respuestas.indexOf(op));
            eRespuesta.addEventListener("change", Test.cambioRespuesta, false);
            //asigno etiqueta label al checkbox
            eLabel = document.createElement("label");
            eLabel.setAttribute("class", "lbl-respuesta");
            eLabel.setAttribute("for", "respuesta-" + indexPreguntaReal);
            eLabel.innerHTML = op;
            //lo agrego al panel de la pregunta
            panelPregunta.appendChild(eRespuesta);
            panelPregunta.appendChild(eLabel);
        }

        return panelPregunta;
    },

    cambioRespuesta: function (evt) {
        respuesta = evt.currentTarget;
        //nroPreg dentro del array de seleccionadas (no me interesa el nro real ya que ya me guarde todo lo que quería)
        nroPreg = respuesta.getAttribute("name").split("-")[1];
        nroResp = respuesta.getAttribute("value");
        if (respuesta.checked) {
            Test.respuestas[nroPreg].push(nroResp);
        } else {
            Test.respuestas[nroPreg].splice(Test.respuestas[nroPreg].indexOf(nroResp), 1);
        }
    }
}
